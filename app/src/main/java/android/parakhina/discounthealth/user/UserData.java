package android.parakhina.discounthealth.user;

public class UserData {
    private static String id;
    private static String name;
    private static String surname;
    private static String email;
    private static String bday;
    private static String date;
    private static String country;
    private static int points;
    private static int insId;

    public UserData(String id, String name, String surname, String email, String dob, String date, String country, int points, int insId) {
        UserData.id = id;
        UserData.name = name;
        UserData.surname = surname;
        UserData.email = email;
        UserData.bday = dob;
        UserData.date = date;
        UserData.country = country;
        UserData.points = points;
        UserData.insId = insId;
    }

    public static String getId() {
        return id;
    }

    public static void setId(String id) {
        UserData.id = id;
    }

    public static String getName() {
        return name;
    }

    public static void setName(String name) {
        UserData.name = name;
    }

    public static String getSurname() {
        return surname;
    }

    public static void setSurname(String surname) {
        UserData.surname = surname;
    }

    public static String getEmail() {
        return email;
    }

    public static void setEmail(String email) {
        UserData.email = email;
    }

    public static String getBday() {
        return bday;
    }

    public static void setBday(String bday) {
        UserData.bday = bday;
    }

    public static String getDate() {
        return date;
    }

    public static void setDate(String date) {
        UserData.date = date;
    }

    public static String getCountry() {
        return country;
    }

    public static void setCountry(String country) {
        UserData.country = country;
    }
    public static int getPoints() {
        return points;
    }

    public static void setPoints(int points) {
        UserData.points = points;
    }

    public static int getInsId() {
        return insId;
    }

    public static void setInsId(int insId) {
        UserData.insId = insId;
    }

}
