package android.parakhina.discounthealth.companies;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Company {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("count(insurance.id)")
    @Expose
    private String countInsurance;
    @SerializedName("min(insurance.price)")
    @Expose
    private String minInsurancePrice;
    @SerializedName("max(insurance.price)")
    @Expose
    private String maxInsurancePrice;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("lat")
    @Expose
    private Double lat;
    @SerializedName("lng")
    @Expose
    private Double lng;

    @SerializedName("rate")
    @Expose
    private float rate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountInsurance() {
        return countInsurance;
    }

    public void setCountInsurance(String countInsurance) {
        this.countInsurance = countInsurance;
    }

    public String getMinInsurancePrice() {
        return minInsurancePrice;
    }

    public void setMinInsurancePrice(String minInsurancePrice) {
        this.minInsurancePrice = minInsurancePrice;
    }

    public String getMaxInsurancePrice() {
        return maxInsurancePrice;
    }

    public void setMaxInsurancePrice(String maxInsurancePrice) {
        this.maxInsurancePrice = maxInsurancePrice;
    }
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

     public Company(int id, String name, String minInsurancePrice, String maxInsurancePrice, String countInsurance) {
        this.id = id;
        this.name = name;
         this.countInsurance = countInsurance;
         this.minInsurancePrice = minInsurancePrice;
         this.maxInsurancePrice = maxInsurancePrice;
     }
    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }




}
