package android.parakhina.discounthealth.retrofit;

import android.parakhina.discounthealth.companies.Companies;
import android.parakhina.discounthealth.companies.Company;
import android.parakhina.discounthealth.discounts.Discounts;
import android.parakhina.discounthealth.insurances.Insurances;
import android.parakhina.discounthealth.user.User;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiService {
    @FormUrlEncoded
    @POST("/users/postuser")
    Call<User> createUser(
            @Field("name") String name,
            @Field("surname") String surname,
            @Field("email") String email,
            @Field("password") String password,
            @Field("month") String month,
            @Field("day") String day,
            @Field("year") String year,
            @Field("date") String regDate,
            @Field("country") String country
    );
    @FormUrlEncoded
    @POST("/users/checkemail")
    Call<User> checkEmail(
            @Field("email") String email
    );
    @FormUrlEncoded
    @POST("/users/checkpass")
    Call<User> checkPass(
            @Field("email") String email,
            @Field("password") String password
    );
    @FormUrlEncoded
    @POST("/users/login")
    Call<User> loginUser(
            @Field("email") String email
    );
    @FormUrlEncoded
    @POST("/companies")
    Call<Companies> getCompanies(
            @Field("name") String name
    );
    @FormUrlEncoded
    @POST("/companies/info")
    Call<Company> compInfo(
            @Field("id") int id
    );
    @FormUrlEncoded
    @POST("/insurances/mobile")
    Call<Insurances> getIns(
            @Field("age") int age,
            @Field("ic_id") int ic_id
    );
    @FormUrlEncoded
    @POST("/discounts/users")
    Call<Discounts> getDisc(
            @Field("points") int points,
            @Field("ins_id") int ins_id
    );
    @FormUrlEncoded
    @POST("/insurances/info")
    Call<Insurances> insInfo(
            @Field("id") int id
    );
    @FormUrlEncoded
    @POST("/insurances/users")
    Call<Insurances> usersInfo(
            @Field("ins_id") int id
    );
    @FormUrlEncoded
    @POST("/rating/post")
    Call<User> setRating(
            @Field("id_user") String user_id,
            @Field("ic_id") int ic_id,
            @Field("rate") float rate
    );
    @FormUrlEncoded
    @POST("/insurances/company")
    Call<Company> insCompany(
            @Field("id") int id
    );
    @FormUrlEncoded
    @POST("/users/setins")
    Call<User> setInsurance(
            @Field("id") String id,
            @Field("ins_id") int ins_id,
            @Field("points") int points
    );
}
