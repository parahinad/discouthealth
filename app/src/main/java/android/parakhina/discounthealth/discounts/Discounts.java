package android.parakhina.discounthealth.discounts;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Discounts {

    @SerializedName("discounts")
    @Expose
    private List<Discount> discounts = null;

    public List<Discount> getDiscounts() {
        return discounts;
    }

    public void setDiscounts(List<Discount> discounts) {
        this.discounts = discounts;
    }

}