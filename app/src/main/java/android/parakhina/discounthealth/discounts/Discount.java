package android.parakhina.discounthealth.discounts;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Discount {

    @SerializedName("Id")
    @Expose
    private int id;
    @SerializedName("Ins_id")
    @Expose
    private int insId;
    @SerializedName("Points")
    @Expose
    private int points;
    @SerializedName("Discount_value")
    @Expose
    private int discountValue;
    private double price;

    public Discount(int id, int insId, int discountValue, int points, double price) {
        this.id = id;
        this.insId = insId;
        this.discountValue = discountValue;
        this.points = points;
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getInsId() {
        return insId;
    }

    public void setInsId(int insId) {
        this.insId = insId;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getDiscountValue() {
        return discountValue;
    }

    public void setDiscountValue(int discountValue) {
        this.discountValue = discountValue;
    }

}