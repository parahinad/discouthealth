package android.parakhina.discounthealth.ui;

import android.content.Intent;
import android.os.Bundle;
import android.parakhina.discounthealth.R;
import android.parakhina.discounthealth.user.UserData;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class ProfileFragment extends Fragment {
    private TextView name, points, email, dob, address;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_profile, container, false);
        name = v.findViewById(R.id.uname);
        points = v.findViewById(R.id.points);
        email = v.findViewById(R.id.email_text);
        dob = v.findViewById(R.id.dob_text);
        address = v.findViewById(R.id.address_text);
        ImageView logout = v.findViewById(R.id.logout);
        logout.setOnClickListener(v1 -> {
            new UserData("", "", "", "", "", "", "", 0, 0);
            Intent intent = new Intent(getActivity(), MainActivity.class);
            startActivity(intent);
        });
        getData();
        return v;
    }
    private void getData(){
        String full_name = UserData.getName() + " " + UserData.getSurname();
        name.setText(full_name);
        email.setText(UserData.getEmail());
        dob.setText(UserData.getBday());
        address.setText(UserData.getCountry());
        points.setText(String.valueOf(UserData.getPoints()));
    }
}
