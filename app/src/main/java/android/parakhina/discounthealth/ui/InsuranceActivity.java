package android.parakhina.discounthealth.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.parakhina.discounthealth.R;
import android.parakhina.discounthealth.discounts.Discount;
import android.parakhina.discounthealth.discounts.Discounts;
import android.parakhina.discounthealth.insurances.Insurance;
import android.parakhina.discounthealth.insurances.Insurances;
import android.parakhina.discounthealth.retrofit.RetroClient;
import android.parakhina.discounthealth.rv.DiscountRVAdapter;
import android.parakhina.discounthealth.user.UserData;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InsuranceActivity extends AppCompatActivity {

    private TextView name_tv, desc_tv, price_tv, amount_tv;
    private RecyclerView rv;
    private List<Discount> discounts;
    private double insPrice;
    Button mButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insurance);
        Intent intent = getIntent();
        int insId = intent.getIntExtra("ins_id", 0);
        name_tv = (TextView) findViewById(R.id.name_text);
        desc_tv = (TextView) findViewById(R.id.desc_text);
        price_tv = (TextView) findViewById(R.id.price_text);
        amount_tv = (TextView) findViewById(R.id.amount_text);
        getInfo(insId);
        rv = findViewById(R.id.ins_rv);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);
        getData(this, insId);
        mButton = (Button)findViewById(R.id.b_wo_disc);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager manager = getSupportFragmentManager();
                MyDialogFragment myDialogFragment = new MyDialogFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("points", 0);
                bundle.putInt("ins_id", insId);
                myDialogFragment.setArguments(bundle);
                myDialogFragment.show(manager, "myDialog");
            }
        });

    }
    private void getInfo(int id){
        RetroClient.getInstance()
                .getApiService()
                .insInfo(id)
                .enqueue(new Callback<Insurances>() {
                    @Override
                    public void onResponse(Call<Insurances> call, Response<Insurances> response) {
                        if (!response.isSuccessful()) {
                            System.out.println(response.code());
                            return;
                        }
                        assert response.body() != null;
                        List<Insurance> mInsurancesList = response.body().getInsurances();
                        for (Insurance insurance : mInsurancesList) {
                            insPrice = insurance.getPrice();
                            name_tv.setText(insurance.getName());
                            desc_tv.setText(insurance.getDescription());
                            price_tv.setText(String.format(Locale.getDefault(), "%.2f", insurance.getPrice()));
                            amount_tv.setText(String.format(Locale.getDefault(), "%.2f", insurance.getInsuranceAmount()));
                        }
                    }
                    @Override
                    public void onFailure(Call<Insurances> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
    }

    private void getData(Context context, int id) {
        Register reg = new Register();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.MM.yyyy");
        int uAge  = reg.calculateAge(LocalDate.parse(UserData.getBday(), formatter));
        RetroClient.getInstance()
                .getApiService()
                .getDisc(UserData.getPoints(), id)
                .enqueue(new Callback<Discounts>() {
                    @Override
                    public void onResponse(Call<Discounts> call, Response<Discounts> response) {
                        if (!response.isSuccessful()) {
                            System.out.println(response.code());
                            return;
                        }
                        discounts = new ArrayList<>();
                        double price;
                        List<Discount> mDiscountList = response.body().getDiscounts();
                        for (Discount discount : mDiscountList) {
                            price = insPrice - insPrice * ((float)discount.getDiscountValue()/100);
                            System.out.println(price);
                            discounts.add(new Discount(discount.getId(), discount.getInsId(), discount.getDiscountValue(), discount.getPoints(), insPrice - insPrice * ((float)discount.getDiscountValue()/100)));
                        }
                        DiscountRVAdapter adapter = new DiscountRVAdapter(context, discounts);
                        rv.setAdapter(adapter);
                    }

                    @Override
                    public void onFailure(Call<Discounts> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
    }

}
