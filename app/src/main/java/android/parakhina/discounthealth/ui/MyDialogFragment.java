package android.parakhina.discounthealth.ui;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.parakhina.discounthealth.R;
import android.parakhina.discounthealth.retrofit.RetroClient;
import android.parakhina.discounthealth.rv.CompanyRVAdapter;
import android.parakhina.discounthealth.user.User;
import android.parakhina.discounthealth.user.UserData;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyDialogFragment extends DialogFragment {
    Activity mActivity;
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle args = getArguments();
        int points = args.getInt("points");
        int ins_id = args.getInt("ins_id");
        String title = getResources().getString(R.string.dialog_title);
        String message;
        if(points == 0){
            message = getResources().getString(R.string.dialog_text_wo);
        }else{
            message = getResources().getString(R.string.dialog_text);
        }
        String button1String = getResources().getString(R.string.dialog_ok);
        String button2String = getResources().getString(R.string.dialog_cancel);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title);  // заголовок
        builder.setMessage(message); // сообщение
        builder.setPositiveButton(button1String, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if(UserData.getInsId() != 0){
                    Toast.makeText(getActivity(), getResources().getString(R.string.have_insurance), Toast.LENGTH_SHORT).show();
                }else{
                    setRating(ins_id, points);
                }

            }
        });
        builder.setNegativeButton(button2String, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        mActivity =getActivity();
        builder.setCancelable(true);

        return builder.create();
    }

    private void setRating(int ins_id, int points){
        RetroClient.getInstance()
                .getApiService()
                .setInsurance(UserData.getId(), ins_id, points)
                .enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {
                        if (!response.isSuccessful()) {
                            System.out.println(response.code());
                            return;
                        }
                        if(Objects.requireNonNull(response.body()).getMessage().equals("Ok")) {
                            UserData.setInsId(ins_id);
                            CompanyRVAdapter.icId = -1;
                            UserData.setPoints(UserData.getPoints() - points);
                            Intent intent = new Intent(getActivity(), Content.class);
                            intent.putExtra("status", "Ok");
                            mActivity.startActivity(intent);
                        }
                    }
                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
    }
}