package android.parakhina.discounthealth.ui;

import android.content.Intent;
import android.os.Bundle;
import android.parakhina.discounthealth.R;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.Objects;

public class Content extends AppCompatActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content);
        BottomNavigationView mBottomNavigationView = findViewById(R.id.navigation);
        mBottomNavigationView.setOnNavigationItemSelectedListener(mNavigationItemSelectedListener);
        Intent intent = getIntent();
        int icId = intent.getIntExtra("ic_id", -1);
        String status = intent.getStringExtra("status");
        if(icId != -1){
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new CompanyFragment()).commit();
        }else if(status != null && status.equals("Ok")){
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new BookedFragment()).commit();
        }else {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ProfileFragment()).commit();
        }
    }
    private BottomNavigationView.OnNavigationItemSelectedListener mNavigationItemSelectedListener = menuItem -> {
        Fragment selectedFragment = null;
        switch (menuItem.getItemId()){
            case R.id.action_search:
                selectedFragment = new SearchFragment();
                break;
            case R.id.action_booked:
                selectedFragment = new BookedFragment();
                break;
            case R.id.action_profile:
                selectedFragment = new ProfileFragment();
                break;
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, Objects.requireNonNull(selectedFragment)).commit();
        return true;
    };
}
