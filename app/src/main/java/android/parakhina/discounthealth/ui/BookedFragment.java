package android.parakhina.discounthealth.ui;

import android.content.Context;
import android.os.Bundle;
import android.parakhina.discounthealth.R;
import android.parakhina.discounthealth.insurances.Insurance;
import android.parakhina.discounthealth.insurances.Insurances;
import android.parakhina.discounthealth.retrofit.RetroClient;
import android.parakhina.discounthealth.rv.InsuranceRVAdapter;
import android.parakhina.discounthealth.user.UserData;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BookedFragment extends Fragment {

    private RecyclerView rv;
    private List<Insurance> insurances;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View sv = inflater.inflate(R.layout.fragment_booked, container, false);
        rv = sv.findViewById(R.id.b_rv);
        Context context = sv.getContext();
        LinearLayoutManager llm = new LinearLayoutManager(context);
        rv.setLayoutManager(llm);
        getData(getContext());
        return sv;
    }
    private void getData(Context context) {
        RetroClient.getInstance()
                .getApiService()
                .usersInfo(UserData.getInsId())
                .enqueue(new Callback<Insurances>() {
                    @Override
                    public void onResponse(Call<Insurances> call, Response<Insurances> response) {
                        if (!response.isSuccessful()) {
                            System.out.println(response.code());
                            return;
                        }
                        insurances = new ArrayList<>();
                        List<Insurance> mInsurancesList = response.body().getInsurances();
                        for (Insurance insurance : mInsurancesList) {
                            insurances.add(new Insurance(insurance.getId(), insurance.getName(), insurance.getPrice(), insurance.getInsuranceAmount(), insurance.getDescription()));
                        }
                        InsuranceRVAdapter adapter = new InsuranceRVAdapter(context, insurances, true);
                        rv.setAdapter(adapter);
                    }

                    @Override
                    public void onFailure(Call<Insurances> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
    }
}
