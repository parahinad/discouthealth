package android.parakhina.discounthealth.ui;

import android.os.Bundle;
import android.parakhina.discounthealth.R;
import android.parakhina.discounthealth.companies.Company;
import android.parakhina.discounthealth.retrofit.RetroClient;
import android.parakhina.discounthealth.user.User;
import android.parakhina.discounthealth.user.UserData;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RatingActivity extends AppCompatActivity {

    private RatingBar ratingBar;
    private int ic_id;
    private TextView ic_name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating);
        ic_name = (TextView) findViewById(R.id.ic_name);
        ratingBar = (RatingBar) findViewById(R.id.ic_rating);
        getData();
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                setRating(rating);
            }
        });
    }


    private void setRating(float rate){
        RetroClient.getInstance()
                .getApiService()
                .setRating(UserData.getId(), ic_id, rate)
                .enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {
                        if (!response.isSuccessful()) {
                            System.out.println(response.code());
                            return;
                        }
                        if(Objects.requireNonNull(response.body()).getMessage().equals("Ok")) {
                            ratingBar.setIsIndicator(true);
                        }
                    }
                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
    }
    private void getData() {
        RetroClient.getInstance()
                .getApiService()
                .insCompany(UserData.getInsId())
                .enqueue(new Callback<Company>() {
                    @Override
                    public void onResponse(Call<Company> call, Response<Company> response) {
                        if (!response.isSuccessful()) {
                            System.out.println(response.code());
                            return;
                        }
                        assert response.body() != null;
                        ic_id = response.body().getId();
                        ic_name.setText(response.body().getName());
                    }

                    @Override
                    public void onFailure(Call<Company> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
    }


}
