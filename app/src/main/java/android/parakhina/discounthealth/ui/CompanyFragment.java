package android.parakhina.discounthealth.ui;

import android.content.Context;
import android.os.Bundle;
import android.parakhina.discounthealth.R;
import android.parakhina.discounthealth.companies.Company;
import android.parakhina.discounthealth.insurances.Insurance;
import android.parakhina.discounthealth.insurances.Insurances;
import android.parakhina.discounthealth.retrofit.RetroClient;
import android.parakhina.discounthealth.rv.CompanyRVAdapter;
import android.parakhina.discounthealth.rv.InsuranceRVAdapter;
import android.parakhina.discounthealth.user.UserData;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.parakhina.discounthealth.support.Constraints.MAPVIEW_BUNDLE_KEY;

public class CompanyFragment extends Fragment implements OnMapReadyCallback {
    private MapView mMapView;
    private TextView name, email, rate;
    private double lat, lng;
    private RecyclerView rv;
    private List<Insurance> insurances;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_company, container, false);
        name = v.findViewById(R.id.name_text);
        email = v.findViewById(R.id.email_text);
        rate = v.findViewById(R.id.rating);
        mMapView = v.findViewById(R.id.mapView);
        getInfo();
        getData(v.getContext(), CompanyRVAdapter.icId);
        initGoogleMap(savedInstanceState);
        rv = v.findViewById(R.id.ic_rv);
        Context context = v.getContext();
        LinearLayoutManager llm = new LinearLayoutManager(context);
        rv.setLayoutManager(llm);
        return v;
    }

    private void getInfo(){
        RetroClient.getInstance()
                .getApiService()
                .compInfo(CompanyRVAdapter.icId)
                .enqueue(new Callback<Company>() {
                    @Override
                    public void onResponse(Call<Company> call, Response<Company> response) {
                        if (!response.isSuccessful()) {
                            System.out.println(response.code());
                            return;
                        }
                        name.setText(response.body().getName());
                        email.setText(response.body().getEmail());
                        if (response.body().getRate() != 0) {
                            rate.setText(String.format(Locale.getDefault(), "%.1f", response.body().getRate()));
                        }else{
                            rate.setText("-");
                        }
                        lat = response.body().getLat();
                        lng = response.body().getLng();
                    }
                    @Override
                    public void onFailure(Call<Company> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
    }

    private void getData(Context context, int id) {
        Register reg = new Register();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.MM.yyyy");
        int uAge  = reg.calculateAge(LocalDate.parse(UserData.getBday(), formatter));
        RetroClient.getInstance()
                .getApiService()
                .getIns(uAge, id)
                .enqueue(new Callback<Insurances>() {
                    @Override
                    public void onResponse(Call<Insurances> call, Response<Insurances> response) {
                        if (!response.isSuccessful()) {
                            System.out.println(response.code());
                            return;
                        }
                        insurances = new ArrayList<>();
                        List<Insurance> mInsurancesList = response.body().getInsurances();
                        for (Insurance insurance : mInsurancesList) {
                            insurances.add(new Insurance(insurance.getId(), insurance.getName(), insurance.getPrice(), insurance.getInsuranceAmount(), insurance.getDescription()));
                        }
                        InsuranceRVAdapter adapter = new InsuranceRVAdapter(context, insurances, false);
                        rv.setAdapter(adapter);
                    }

                    @Override
                    public void onFailure(Call<Insurances> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
    }

    private void initGoogleMap(Bundle savedInstanceState){
        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAPVIEW_BUNDLE_KEY);
        }

        mMapView.onCreate(mapViewBundle);

        mMapView.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap map) {
        LatLng company = new LatLng(lat, lng);
        map.addMarker(new MarkerOptions().position(company));
        map.moveCamera(CameraUpdateFactory.newLatLng(company));
        map.setMinZoomPreference(18);
    }

    @Override
    public void onPause() {
        mMapView.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        mMapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        Bundle mapViewBundle = outState.getBundle(MAPVIEW_BUNDLE_KEY);
        if (mapViewBundle == null) {
            mapViewBundle = new Bundle();
            outState.putBundle(MAPVIEW_BUNDLE_KEY, mapViewBundle);
        }

        mMapView.onSaveInstanceState(mapViewBundle);
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        mMapView.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        mMapView.onStop();
    }

}
