package android.parakhina.discounthealth.ui;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.ResultReceiver;
import android.parakhina.discounthealth.R;
import android.parakhina.discounthealth.user.User;
import android.parakhina.discounthealth.user.UserData;
import android.parakhina.discounthealth.retrofit.RetroClient;
import android.parakhina.discounthealth.support.Constraints;
import android.parakhina.discounthealth.support.FetchAddressIntentService;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import java.time.LocalDate;
import java.time.Period;
import java.util.Calendar;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Register extends AppCompatActivity {
    private EditText name, surname, email, password, birthday;
    private DatePickerDialog.OnDateSetListener mSetListener;
    String res;
    String[] subStr;
    String country;
    private ResultReceiver resultReceiver;
    private static final int REQUEST_CODE_LOCATION_PERMISSION = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        TextView login = findViewById(R.id.login_reg);
        login.setOnClickListener(v -> {
            Intent intent = new Intent(Register.this, Login.class);
            startActivity(intent);
        });
        resultReceiver = new AddressResultReceiver(new Handler());
        Button create = findViewById(R.id.ca_reg);
        name = findViewById(R.id.name_reg);
        surname = findViewById(R.id.sname_reg);
        email = findViewById(R.id.email_reg);
        password = findViewById(R.id.pass_reg);
        birthday = findViewById(R.id.bday_reg);
        birthday.setInputType(InputType.TYPE_NULL);
        birthday.setOnClickListener(v -> {
            Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog dialog = new DatePickerDialog(
                    Register.this,
                    android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                    mSetListener,
                    year, month, day
            );
            dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
            birthday.setError(null);
        });

        mSetListener = (view, year, month, day) -> {
            month = month + 1;
            String date = month + "/" + day + "/" + year;
            birthday.setText(date);
        };
        String date = birthday.getText().toString();
        System.out.println(date);
        create.setOnClickListener(v -> {
            res = "";
            if(ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(Register.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE_LOCATION_PERMISSION);
            }else{
                getLocation();
            }
        });
        name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                name.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        surname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                surname.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                email.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                password.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_LOCATION_PERMISSION && grantResults.length > 0){
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED){
                getLocation();
            }else{
                Toast.makeText(this, "Permission denied.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void  getLocation(){
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(3000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationServices.getFusedLocationProviderClient(Register.this).requestLocationUpdates(locationRequest, new LocationCallback(){
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                LocationServices.getFusedLocationProviderClient(Register.this).removeLocationUpdates(this);
                if(locationResult != null && locationResult.getLocations().size() > 0){
                    int latestLocationIndex = locationResult.getLocations().size() - 1;
                    Location location = new Location("providerNA");
                    location.setLatitude(locationResult.getLocations().get(latestLocationIndex).getLatitude());
                    location.setLongitude(locationResult.getLocations().get(latestLocationIndex).getLongitude());
                    fetchAddressFromLatLong(location);
                }
            }
        }, Looper.getMainLooper());
    }
    private void createUser(String name, String surname, String email, String password, String month, String day, String year, String country) {
        String regDate = LocalDate.now().toString();
        RetroClient.getInstance()
                .getApiService()
                .createUser(name, surname, email, password, month, day, year, regDate, country)
                .enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {
                        if (!response.isSuccessful()) {
                            Toast.makeText(Register.this, response.code(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (Objects.requireNonNull(response.body()).getMessage().equals("Ok")) {
                                login(email);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
    }

    private void submitForm(String country) {
        if (!checkName() || !checkSurname() || !checkPass() || !checkBDay()) {
            return;
        }
        if (!checkEmail(country)) {
            if(!res.isEmpty()) {
                return;
            }
        }

        if(res.equals("false")){
            createUser(name.getText().toString().trim(), surname.getText().toString().trim(), email.getText().toString().trim(), password.getText().toString().trim(), subStr[0], subStr[1], subStr[2], country);
        }

    }

    private boolean checkName() {
        if (!name.getText().toString().trim().matches("^([А-ЯЁ]{1}[а-яё]+)|([A-Z]{1}[a-z]+)$")) {
            name.setError("Check the name. Note: The name must begin with a capital letter.");
//            name.getText().clear();
            return false;
        }
        return true;
    }

    private boolean checkSurname() {
        if (!surname.getText().toString().trim().matches("^([А-ЯЁ]{1}[а-яё]+)|([A-Z]{1}[a-z]+)$")) {
           surname.setError("Check the surname. Note: The surname must begin with a capital letter.");
//           surname.getText().clear();
           return false;
        }
        return true;
    }

    private boolean checkEmail(String address) {
        String emVal = email.getText().toString().trim();
        if (emVal.isEmpty() || !isValidEmail(emVal)) {
            email.setError("Enter a valid email.");
//            email.getText().clear();
            return false;
        }
        if(res.isEmpty()){
            existEmail(emVal, address);
            return false;
        }
        else if (res.equals("true")) {
            email.setError("A user with such email is already exists.");
//           email.getText().clear();
            return false;
        } else if(res.equals("Error")){
            email.setError("Check your Internet connection!");
//            email.getText().clear();
            return false;
        }
        return true;
    }

    private void existEmail(String email, String address){
        RetroClient.getInstance()
                .getApiService()
                .checkEmail(email)
                .enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {
                        if (!response.isSuccessful()) {
                            Toast.makeText(Register.this, response.code(), Toast.LENGTH_SHORT).show();
                            return;
                        }
                        if(Objects.requireNonNull(response.body()).getMessage().equals("true")) {
                            res = "true";
                        }else{
                            res = "false";
                        }
                        submitForm(address);
                    }
                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        t.printStackTrace();
                        res = "Error";
                    }
                });
    }
    private boolean checkPass(){
        if(!password.getText().toString().trim().matches("^([0-9a-zA-Z!@#$%^&*]{8,})$")){
            password.setError("Your password must be at least 8 characters.");
//           password.getText().clear();
            return false;
        }
        return true;
    }
    private boolean checkBDay() {
        String bday = birthday.getText().toString().trim();
        subStr = bday.split("/", 3);
        if (bday.equals("")){
            birthday.setError("This field cannot be blank.");
//            birthday.getText().clear();
            return false;
        }
        LocalDate birthdate = LocalDate.of(Integer.parseInt(subStr[2]), Integer.parseInt(subStr[0]), Integer.parseInt(subStr[1]));

        if (calculateAge(birthdate) < 18) {
            birthday.setError("You need to be 18 years old.");
//            birthday.getText().clear();
            return false;
        }
        return true;
    }
    private static boolean isValidEmail(String email){
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
    public int calculateAge(LocalDate birthDate) {
        LocalDate currentDate = LocalDate.now();
        if ((birthDate != null) && (currentDate != null)) {
            return Period.between(birthDate, currentDate).getYears();
        } else {
            return 0;
        }
    }
    private void login(String email){
        RetroClient.getInstance()
                .getApiService()
                .loginUser(email)
                .enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {
                        if (!response.isSuccessful()) {
                            Toast.makeText(Register.this, response.code(), Toast.LENGTH_SHORT).show();
                            return;
                        }
                        new UserData(Objects.requireNonNull(response.body()).getId(), response.body().getName(), response.body().getSurname(), response.body().getEmail(), response.body().getDob(), response.body().getDate(), response.body().getCountry(), response.body().getPoints(), response.body().getInsId());
                        Intent intent = new Intent(Register.this, Content.class);
                        startActivity(intent);
                    }
                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        t.printStackTrace();
                        res = "Exception";
                    }
                });
    }

    private void fetchAddressFromLatLong(Location location){
        Intent intent = new Intent(this, FetchAddressIntentService.class);
        intent.putExtra(Constraints.RECEIVER, resultReceiver);
        intent.putExtra(Constraints.LOCATION_DATA_EXTRA, location);
        startService(intent);
    }

    private class AddressResultReceiver extends ResultReceiver{

        AddressResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            super.onReceiveResult(resultCode, resultData);
            if (resultCode == Constraints.SUCCESS_RESULT){
                country = resultData.getString(Constraints.RESULT_DATA_KEY);
                submitForm(country);
            }else{
                Toast.makeText(Register.this, resultData.getString(Constraints.RESULT_DATA_KEY), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
