package android.parakhina.discounthealth.ui;

import android.content.Intent;
import android.os.Bundle;
import android.parakhina.discounthealth.R;
import android.parakhina.discounthealth.retrofit.RetroClient;
import android.parakhina.discounthealth.user.User;
import android.parakhina.discounthealth.user.UserData;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity {
    private EditText email, password;
    String res;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Button reg = findViewById(R.id.ca_login);
        email = findViewById(R.id.email_login);
        password = findViewById(R.id.pass_login);
        reg.setOnClickListener(v -> {
            Intent intent = new Intent(Login.this, Register.class);
            startActivity(intent);
        });
        Button mLoginButton = findViewById(R.id.login_btn);
        mLoginButton.setOnClickListener(v -> {
            res = "";
            submitForm();
        });
        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                email.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                password.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
    private void submitForm() {
        if (!checkEmail() || !checkPass()) {
            return;
        }
        if (res.equals("true")) {
            login(email.getText().toString().trim());
        }

    }

    private boolean checkEmail() {
        String emVal = email.getText().toString().trim();
        if (emVal.isEmpty() || !isValidEmail(emVal)) {
            email.getText().clear();
            password.getText().clear();
            email.setError("Enter a valid email.");
            return false;
        }
        if (res.equals("false")) {
            email.getText().clear();
            password.getText().clear();
            email.setError("User with such data does not exist.");
            return false;
        } else if(res.equals("Exception")){
            email.getText().clear();
            password.getText().clear();
            email.setError("Check your Internet connection!");
            return false;
        }
        return true;
    }
    private void login(String email){
        RetroClient.getInstance()
                .getApiService()
                .loginUser(email)
                .enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {
                        if (!response.isSuccessful()) {
                            Toast.makeText(Login.this, response.code(), Toast.LENGTH_SHORT).show();
                            return;
                        }
                        System.out.println(response.body().getPoints());
                        new UserData(Objects.requireNonNull(response.body()).getId(), response.body().getName(), response.body().getSurname(), response.body().getEmail(), response.body().getDob(), response.body().getDate(), response.body().getCountry(), response.body().getPoints(), response.body().getInsId());
                        Intent intent = new Intent(Login.this, Content.class);
                        startActivity(intent);
                    }
                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        t.printStackTrace();
                        res = "Exception";
                    }
                });
    }

    private void checkPassReq(String email, String pass){
        RetroClient.getInstance()
                .getApiService()
                .checkPass(email, pass)
                .enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {
                        if (!response.isSuccessful()) {
                            System.out.println(response.code());
                            return;
                        }
                        if(Objects.requireNonNull(response.body()).getMessage().equals("true")) {
                            res = "true";
                        }else if (response.body().getMessage().equals("Error")){
                            res = "Error";
                        }else {
                            res = "false";
                        }
                        submitForm();
                    }
                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        t.printStackTrace();
                        res = "Exception";
                    }
                });
    }
    private boolean checkPass(){
        if(!password.getText().toString().trim().matches("^([0-9a-zA-Z!@#$%^&*]{8,})$")){
            password.getText().clear();
            password.setError("Your password must be at least 8 characters.");

            return false;
        }
        if(res.isEmpty()){
            checkPassReq(email.getText().toString().trim(), password.getText().toString().trim());
            //tyt bydet zagruzka kadata
            return false;
        }
        if (res.equals("Error")) {
            password.getText().clear();
            password.setError("Password is incorrect.");

            return false;
        }
        return true;
    }
    private static boolean isValidEmail(String email){
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
}
