package android.parakhina.discounthealth.ui;

import android.content.Context;
import android.os.Bundle;
import android.parakhina.discounthealth.companies.Companies;
import android.parakhina.discounthealth.companies.Company;
import android.parakhina.discounthealth.R;
import android.parakhina.discounthealth.retrofit.RetroClient;
import android.parakhina.discounthealth.rv.CompanyRVAdapter;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchFragment extends Fragment {

    private static String ic_name = "";
    private EditText search;
    private RecyclerView rv;
    private List<Company> companies;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View sv = inflater.inflate(R.layout.fragment_search, container, false);

        search = (EditText) sv.findViewById(R.id.search_ic);
        search.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                        case KeyEvent.KEYCODE_ENTER:
                            // hide keyboard
                            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(search.getWindowToken(),
                                    InputMethodManager.HIDE_NOT_ALWAYS);
                            return true;
                        default:
                            break;
                    }
                }
                return false;
            }
        });
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                ic_name = "";
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ic_name = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
                ic_name = s.toString();
                getData(getContext(), ic_name);
            }
        });
        rv = sv.findViewById(R.id.rv);
        Context context = sv.getContext();
        LinearLayoutManager llm = new LinearLayoutManager(context);
        rv.setLayoutManager(llm);
        getData(getContext(), ic_name);

        return sv;
    }

    private void getData(Context context, String ic_name) {
        RetroClient.getInstance()
                .getApiService()
                .getCompanies(ic_name)
                .enqueue(new Callback<Companies>() {
                    @Override
                    public void onResponse(Call<Companies> call, Response<Companies> response) {
                        if (!response.isSuccessful()) {
                            System.out.println(response.code());
                            return;
                        }
                        companies = new ArrayList<>();
                        assert response.body() != null;
                        List<Company> mCompaniesList = response.body().getCompanies();
                        for (Company company : mCompaniesList) {
                            companies.add(new Company(company.getId(), company.getName(), company.getMinInsurancePrice(), company.getMaxInsurancePrice(), company.getCountInsurance()));
                        }
                        CompanyRVAdapter adapter = new CompanyRVAdapter(context, companies);
                        rv.setAdapter(adapter);
                    }

                    @Override
                    public void onFailure(Call<Companies> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
    }
}
