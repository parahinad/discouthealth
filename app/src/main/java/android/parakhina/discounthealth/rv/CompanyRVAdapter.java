package android.parakhina.discounthealth.rv;

import android.content.Context;
import android.content.Intent;
import android.parakhina.discounthealth.R;
import android.parakhina.discounthealth.companies.Company;
import android.parakhina.discounthealth.ui.Content;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class CompanyRVAdapter extends RecyclerView.Adapter<CompanyRVAdapter.CompanyViewHolder>{
    private Context context;
    public static int icId;
    public CompanyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.ic_card, viewGroup, false);

        CompanyViewHolder cvh = new CompanyViewHolder(v);
        return cvh;
    }
    private List<Company> company;
    public CompanyRVAdapter(Context context, List<Company> company){
        this.context = context;
        this.company = company;
    }
    @Override
    public void onBindViewHolder(@NonNull CompanyViewHolder holder, int position) {
        holder.cv.setId(company.get(position).getId());
        holder.name_tv.setText(company.get(position).getName());
        holder.min_price_tv.setText(company.get(position).getMinInsurancePrice());
        holder.max_price_tv.setText(company.get(position).getMaxInsurancePrice());
        holder.ins_num_tv.setText(company.get(position).getCountInsurance());
        holder.cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                icId = holder.cv.getId();
                Intent intent = new Intent(context, Content.class);
                intent.putExtra("ic_id", holder.cv.getId());
                context.startActivity(intent);
            }
        });
    }
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        return company.size();
    }

    class CompanyViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView name_tv, min_price_tv, max_price_tv, ins_num_tv;

        CompanyViewHolder(View itemView) {
            super(itemView);
            cv = (CardView) itemView.findViewById(R.id.iccv);
            name_tv = (TextView) itemView.findViewById(R.id.ic_name_text);
            min_price_tv = (TextView) itemView.findViewById(R.id.min_pr_text);
            max_price_tv = (TextView) itemView.findViewById(R.id.max_pr_text);
            ins_num_tv = (TextView) itemView.findViewById(R.id.num_ins_text);
        }


    }

}
