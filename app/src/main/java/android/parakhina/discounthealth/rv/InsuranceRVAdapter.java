package android.parakhina.discounthealth.rv;

import android.content.Context;
import android.content.Intent;
import android.parakhina.discounthealth.R;
import android.parakhina.discounthealth.insurances.Insurance;
import android.parakhina.discounthealth.ui.InsuranceActivity;
import android.parakhina.discounthealth.ui.RatingActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.Locale;

public class InsuranceRVAdapter extends RecyclerView.Adapter<InsuranceRVAdapter.InsuranceViewHolder> {
    private Context context;
    private boolean flag;

    @NonNull
    public InsuranceViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.ins_card, viewGroup, false);

        InsuranceViewHolder ivh = new InsuranceViewHolder(v);
        return ivh;
    }

    private List<Insurance> insurance;

    public InsuranceRVAdapter(Context context, List<Insurance> insurance, boolean flag) {
        this.context = context;
        this.insurance = insurance;
        this.flag = flag;
    }

    @Override
    public void onBindViewHolder(@NonNull InsuranceViewHolder holder, int position) {
        holder.cv.setId(insurance.get(position).getId());
        holder.name_tv.setText(insurance.get(position).getName());
        holder.amount_tv.setText(String.format(Locale.getDefault(), "%.2f", insurance.get(position).getInsuranceAmount()));
        holder.price_tv.setText(String.format(Locale.getDefault(), "%.2f", insurance.get(position).getPrice()));
        holder.cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                if (!flag) {
                    intent = new Intent(context, InsuranceActivity.class);
                } else {
                    intent = new Intent(context, RatingActivity.class);
                }
                intent.putExtra("ins_id", holder.cv.getId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        return insurance.size();
    }

    class InsuranceViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView name_tv, amount_tv, price_tv;

        InsuranceViewHolder(View itemView) {
            super(itemView);
            cv = (CardView) itemView.findViewById(R.id.inscv);
            name_tv = (TextView) itemView.findViewById(R.id.ins_name_text);
            amount_tv = (TextView) itemView.findViewById(R.id.amount_text);
            price_tv = (TextView) itemView.findViewById(R.id.price_text);
        }
    }
}
