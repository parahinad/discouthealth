package android.parakhina.discounthealth.rv;

import android.content.Context;
import android.os.Bundle;
import android.parakhina.discounthealth.R;
import android.parakhina.discounthealth.discounts.Discount;
import android.parakhina.discounthealth.ui.MyDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.Locale;

public class DiscountRVAdapter extends RecyclerView.Adapter<DiscountRVAdapter.DiscountViewHolder> {
    private Context context;

    @NonNull
    public DiscountViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.disc_card, viewGroup, false);

        DiscountViewHolder dvh = new DiscountViewHolder(v);
        return dvh;
    }

    private List<Discount> discount;

    public DiscountRVAdapter(Context context, List<Discount> discount) {
        this.context = context;
        this.discount = discount;
    }

    @Override
    public void onBindViewHolder(@NonNull DiscountViewHolder holder, int position) {
        holder.cv.setId(discount.get(position).getId());
        holder.points_tv.setText(String.valueOf(discount.get(position).getPoints()));
        holder.discount_tv.setText(String.valueOf(discount.get(position).getDiscountValue()));
        holder.price_tv.setText(String.format(Locale.getDefault(), "%.2f", discount.get(position).getPrice()));
        holder.cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager manager = ((AppCompatActivity)context).getSupportFragmentManager();
                MyDialogFragment myDialogFragment = new MyDialogFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("points", discount.get(position).getPoints());
                bundle.putInt("ins_id", discount.get(position).getInsId());
                myDialogFragment.setArguments(bundle);
                myDialogFragment.show(manager, "myDialog");
            }
        });
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        return discount.size();
    }

    class DiscountViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView points_tv, discount_tv, price_tv;

        DiscountViewHolder(View itemView) {
            super(itemView);
            cv = (CardView) itemView.findViewById(R.id.disccv);
            points_tv = (TextView) itemView.findViewById(R.id.points_text);
            discount_tv = (TextView) itemView.findViewById(R.id.disc_text);
            price_tv = (TextView) itemView.findViewById(R.id.price_text);
        }
    }
}
