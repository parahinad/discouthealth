package android.parakhina.discounthealth.insurances;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Insurance {

    @SerializedName("Id")
    @Expose
    private int id;
    @SerializedName("Ic_id")
    @Expose
    private int icId;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Price")
    @Expose
    private double price;
    @SerializedName("Age_from")
    @Expose
    private int ageFrom;
    @SerializedName("Age_to")
    @Expose
    private int ageTo;
    @SerializedName("Insurance_amount")
    @Expose
    private double insuranceAmount;
    @SerializedName("Description")
    @Expose
    private String description;

    public Insurance(int id, String name, double price, double insuranceAmount, String description) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.insuranceAmount = insuranceAmount;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIcId() {
        return icId;
    }

    public void setIcId(int icId) {
        this.icId = icId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getAgeFrom() {
        return ageFrom;
    }

    public void setAgeFrom(int ageFrom) {
        this.ageFrom = ageFrom;
    }

    public int getAgeTo() {
        return ageTo;
    }

    public void setAgeTo(int ageTo) {
        this.ageTo = ageTo;
    }

    public double getInsuranceAmount() {
        return insuranceAmount;
    }

    public void setInsuranceAmount(double insuranceAmount) {
        this.insuranceAmount = insuranceAmount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}