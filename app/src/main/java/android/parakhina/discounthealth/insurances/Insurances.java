package android.parakhina.discounthealth.insurances;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Insurances {

    @SerializedName("insurances")
    @Expose
    private List<Insurance> insurances = null;

    public List<Insurance> getInsurances() {
        return insurances;
    }

    public void setInsurances(List<Insurance> insurances) {
        this.insurances = insurances;
    }

}